/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fira;

/**
 *
 * @author Sergi
 */
public class AlcadaException extends Exception {
    
    public AlcadaException() {
        super();
    }
    
    public AlcadaException(String msg) {
        super(msg);
    }
}
