/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fira;

import java.util.Random;

/**
 *
 * @author Sergi
 */
public class PortaEntrada {
    private final static int MIN_ALTURA = 150;
    private final static int MAX_ALTURA = 190;
    private int alcada;
    
    public void comprovar(alcada) throws AlcadaException {
               
        if (alcada < MIN_ALTURA) {
            throw new SotaAlcadaException("\"Ets massa baixet per pujar\"");
        } else if (alcada > MAX_ALTURA) {
            throw new SobreAlcadaException("\"Ets massa alt per pujar\"");
        }
        
        System.out.println("\"Pots passar\"");
       
    }

    public int getAlcada() {
        Random rnd = new Random();
        int min = 130;
        int max = 216;
        alcada = rnd.nextInt(max-min) + min;
        
        return alcada;
    }

    public void setAlcada(int alcada) {
        this.alcada = alcada;
    }
    
    
}
