package net.infobosccoma.address.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.infobosccoma.address.MainApp;
import net.infobosccoma.address.model.Person;
import net.infobosccoma.address.util.DateUtil;

public class PersonOverviewController {
    @FXML
    private TableView<Person> personTable;
    @FXML
    private TableColumn<Person, String> firstNameColumn;
    @FXML
    private TableColumn<Person, String> lastNameColumn;

    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label streetLabel;
    @FXML
    private Label postalCodeLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label birthdayLabel;

    // Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public PersonOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
	
	showPersonDetails(null);
	personTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        personTable.setItems(mainApp.getPersonData());
    }
    
    private void showPersonDetails(Person person) {
	if (person != null) {
	    // Fill the labels with info from the person object.
	    firstNameLabel.setText(person.getFirstName());
	    lastNameLabel.setText(person.getLastName());
	    streetLabel.setText(person.getStreet());
	    postalCodeLabel.setText(person.getPostalCode());
	    cityLabel.setText(person.getCity());
	    birthdayLabel.setText(DateUtil.format(person.birthdayObject()));
	} else {
	    // Person is null, remove all the text.
	    firstNameLabel.setText("");
	    lastNameLabel.setText("");
	    streetLabel.setText("");
	    postalCodeLabel.setText("");
	    cityLabel.setText("");
	    birthdayLabel.setText("");
	}
    }
    @FXML
    private void deletePerson() {
	int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
	if(selectedIndex >= 0) {
	    personTable.getItems().remove(selectedIndex);//afegir al on action del boto borrar (nom metode)
	} else {
	    Alert alert = new Alert(AlertType.INFORMATION);
	    alert.setTitle("No selection");
	    alert.setHeaderText("No person selected");
	    alert.setContentText("Please, select a person in the table.");
	    alert.showAndWait();
	}
    }
     /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new person.
     */
    @FXML
    private void handleNewPerson() {
        Person tempPerson = new Person();
        boolean okClicked = mainApp.showPersonEditDialog(tempPerson);
        if (okClicked) {
            mainApp.getPersonData().add(tempPerson);
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected person.
     */
    @FXML
    private void handleEditPerson() {
        Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            boolean okClicked = mainApp.showPersonEditDialog(selectedPerson);
            if (okClicked) {
                showPersonDetails(selectedPerson);
            }

        } else {

            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Person Selected");
            alert.setContentText("Please select a person in the table.");
            alert.showAndWait();
        }
    }
}
